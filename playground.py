import random
from sorters.merge import mergesort
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

random_list = [random.randrange(1, 700)
               for item in range(random.randint(1, 100))]

logger.info(msg=f"merge     {mergesort(random_list)}")
